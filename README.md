# station_meteo

Station Météo Arduino  
Carte arduino : Arduino Mega2560

## à faire 

Implémenter le GPS  
aide : https://phmarduino.wordpress.com/2020/07/11/comment-ajouter-rapidement-un-gps-a-un-montage/comment-page-1/  

## Commandes

```
#Cloner le dépot
git clone https://gitlab.com/ArtLF/station_meteo

#status de la branche et des modifications
git status

#pour ajouter les fichiers dans le commit
git add <fichier>...

#commit toutes les modifications (les nouveaux fichiers doivent passer par git add)
git commit -a

#pull avant de push si des modification sont présentes à distances et qu'un merge doit être fait
#ou mettre à jour
git pull

#push les modifications
git push
```


command for git : #man git
```
git confglobal user.name "" #git config --global user.name "test"
git confglobal user.email @ #git config --global user.email "test@test.test"
git config --global core.editor #gedit mousepad etc...
git config --list
git clone --branch GP2 https://gitlab.com/.....
git branch
git branch -r
git status
git pull
git push
git commit -a
git log --oneline --decorate --graph --all
git branch docsDoxygenFreeTab2D
git checkout docsDoxygenFreeTab2D
git checkout GP2
git merge docsDoxygenFreeTab2D
```

Faire un commit avec vi :
```
o 
#écrire le commit
ESC
:
wq

```
Suppr les comptes git sur windows :
Panneau de configuration, credential manager, supprimer les comptes gits

Ajouter les permissions sur linux :
```
ls /dev/ttyACM0 #changer le nom si autre port
sudo chmod a+rw /dev/ttyACM0
```
