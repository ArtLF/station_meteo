// Fichier Calendrier.cpp
// Fonction de manipulation de date et heure dans un calendrier
// Savoir si une année est bissextile
// Définir le jour de la semaine depuis la date jj.mm.aa
// Calculer l'heure locale en fonction de l'heure UTC, du fuseau horaire et de la correction heure Eté - hiver

#include <Arduino.h>
#include "Calendrier.h"
#include "RTC_DS1307.h"

/*--------------------------------------------------------------------------------------------*/
// Fonctions de manipulation de calendrier
/*--------------------------------------------------------------------------------------------*/
// Une année est bissextile si :
//  - Elle est divisible par 4 et n'est pas divisible par 100
//  - Elle est divisible par 400
// Valeur retournée : 1 année bissextile; 0 : année non bissextile
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
bool bissextile (uint16_t year){
  if((year+2000)%4==0 && (year+2000)%100!=0){
    return 1; 
  }
  else if((year+2000)%400==0){
    return 1;
  }
  else{
    return 0;
  }
}
/*--------------------------------------------------------------------------------------------*/
// Calcul du jour de la semaine en fonction de la date
// Entrées :
//  jour : valeur sur {1,...,31}
//  mois : valeur sur {1,...,12}
//  annee : valeur sur {1583,...,9999}. On ne prend en compte que les années 2000 à 2099 
//    dans l'ensemble {00,...,99} soient 2000 à 2099
// Sortie : le jour de la semaine. 
// cf. https://fr.wikibooks.org/wiki/Curiosit%C3%A9s_math%C3%A9matiques/Trouver_le_jour_de_la_semaine_avec_une_date_donn%C3%A9e
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
/*--------------------------------------------------------------------------------------------*/
// Calcul de l'heure locale en fonction de l'heure UTC
// MàJ de la date locale en fonction de la correction GMT et de la correction Eté-Hiver
// Correction_GMT : correction horaire en fonction du fuseau horaire au format hh:mm:00
// Correction_Ete_Hiver : correction heure été - hiver
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
horloge Correction_Heure_Date(horloge DH_UTC, heure Correction_UTC, heure Correction_EteHiver){
  int daysMonth;
  horloge HRLG_locale = DH_UTC;
  
  HRLG_locale.heureRTC.minute = DH_UTC.heureRTC.minute + Correction_UTC.minute + Correction_EteHiver.minute;
  HRLG_locale.heureRTC.hour = DH_UTC.heureRTC.hour + Correction_UTC.hour + Correction_EteHiver.hour;
  
  if (HRLG_locale.heureRTC.minute>=60){
    HRLG_locale.heureRTC.minute=HRLG_locale.heureRTC.minute%60;
    HRLG_locale.heureRTC.hour=HRLG_locale.heureRTC.hour+1;
  }
  if (HRLG_locale.heureRTC.hour>=24){
    HRLG_locale.heureRTC.hour=HRLG_locale.heureRTC.hour%24;
    HRLG_locale.dateRTC.dayOfMonth+=1;
  }

  switch(HRLG_locale.dateRTC.month){
    case 1: case 3: case 5: case 7: case 8: case 10: case 12:
      daysMonth = 31;
      break;
    case 2:
      if(bissextile(HRLG_locale.dateRTC.year)==1){
        daysMonth=29;
      }
      else{
        daysMonth=28;
      }
      break;
    case 4: case 6: case 9: case 11:
      daysMonth = 30;
      break;
  }
  
  if (HRLG_locale.dateRTC.dayOfMonth>daysMonth){
    HRLG_locale.dateRTC.dayOfMonth=1;
    HRLG_locale.dateRTC.month+=1;
  }
  if (HRLG_locale.dateRTC.month>12){
    HRLG_locale.dateRTC.month=1;
    HRLG_locale.dateRTC.year+=1;
  }
  HRLG_locale.dateRTC.dayOfWeek=Jour_Semaine(HRLG_locale.dateRTC.dayOfMonth,HRLG_locale.dateRTC.month,HRLG_locale.dateRTC.year);

  return HRLG_locale;
}
/*--------------------------------------------------------------------------------------------*/
