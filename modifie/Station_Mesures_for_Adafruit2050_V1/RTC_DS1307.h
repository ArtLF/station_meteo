// Fichier RTC_DS1307.h
// Contient les définitions de types de données associées aux fonctions
//  de manipulation du circuit d'horloge RTC DS1307

#include <Arduino.h>
#ifndef __RTC_DS1307_H__
#define __RTC_DS1307_H__

// Définition de type de données pour gestion de la date
typedef struct date {
    uint8_t dayOfWeek;// day of week, 1 = Monday, 7 = Sunday
    uint8_t dayOfMonth;
    uint8_t month;
    uint16_t year;
}date;

// Définition de type de données pour gestion de l'heure
typedef struct heure {
    uint8_t second;
    uint8_t minute;
    uint8_t hour;
}heure;

// Définition de type de données pour gestion de la date et l'heure sur RTC DS1307
typedef struct horloge {
    heure heureRTC;
    date dateRTC;
}horloge;

// Définition de type de données pour gestion des fuseaux horaires


// Définition de constantes symboliques
// Circuit RTC DS1307 : adressage I2C
#define DS1307_I2C_ADDRESS 0x68 // adresse I2C du circuit DS1307 : 110 1000
#define ADR_REG_SECONDE B00000000 // adresse du registre des secondes

// Fonctions prototypes
uint8_t decToBcd(uint8_t val);
uint8_t bcdToDec(uint8_t val);
void setDateDs1307(horloge HRLG);
horloge getDateDs1307();
uint8_t Jour_Semaine( uint8_t d,uint8_t m,uint8_t y);
#endif
