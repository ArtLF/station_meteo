// Fichier Affichage.h
// Routines d'affichage :
//  Sur le terminal série

#include <Arduino.h>
#include "RTC_DS1307.h"
// Définition de constantes symboliques

// Fonctions prototypes
void Affiche_date_heure(horloge HRLG);
void Afficher_jour( uint8_t d,uint8_t m,uint8_t y);
