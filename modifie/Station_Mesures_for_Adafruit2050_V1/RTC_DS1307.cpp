// Fichier RTC_DS1307.cpp
// Fonctions de gestion du circuit d'horloge RTC DS1307
// Affichage de la date et de l'heure sur terminal série : 

// Initialisation des paramètres de l'horloge : 
// Acquisition de la date et de l'heure : 
// 2 fonctions additionnelles :
//  - byte decToBcd(byte val); Conversion Décimal vers BCD
//  - byte bcdToDec(byte val); Conversion BCD vers Décimal


#include <Arduino.h>
#include <Wire.h>
#include "RTC_DS1307.h"

/*--------------------------------------------------------------------------------------------*/
// Fonctions de gestion du circuit DS1307
/*--------------------------------------------------------------------------------------------*/
// Convert normal decimal numbers to binary coded decimal (OK)
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
uint8_t decToBcd(uint8_t val) {
    return ((val / 10 * 16) + (val % 10));
}
/*--------------------------------------------------------------------------------------------*/
// Convert binary coded decimal to normal decimal numbers (OK)
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
uint8_t bcdToDec(uint8_t val) {
    return ((val / 16 * 10) + (val % 16));
}
/*--------------------------------------------------------------------------------------------*/
// Arret du circuit d'horloge DS1307 (OK)
// Mise à 1 du bit 7 (CH) du registre des secondees d'adresse 0x00
// Consequence : Mise à zero des secondes
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
/*--------------------------------------------------------------------------------------------*/
// Initialisation des paramétres de date et heure de l'horloge
// 1) Mise à jour des registres de date et heure du DS1307
// 2) Démarrer l'horloge
// 3) Définir le mode 24h pour l'horloge
/*--------------------------------------------------------------------------------------------*/
// Ici votre code

void setDateDs1307(horloge HRLG) {
    Wire.beginTransmission(DS1307_I2C_ADDRESS);
    Wire.write((uint8_t)0x00);
    Wire.write(decToBcd(HRLG.heureRTC.second));// 0 to bit 7 starts the clock
    Wire.write(decToBcd(HRLG.heureRTC.minute));
    Wire.write(decToBcd(HRLG.heureRTC.hour));  // If you want 12 hour am/pm you need to set bit 6
    Wire.write(decToBcd(HRLG.dateRTC.dayOfWeek));
    Wire.write(decToBcd(HRLG.dateRTC.dayOfMonth));
    Wire.write(decToBcd(HRLG.dateRTC.month));
    Wire.write(decToBcd(HRLG.dateRTC.year));
    Wire.endTransmission();
}
/*--------------------------------------------------------------------------------------------*/
// Lire les registres de l'horloge pour récupérer heure, minutes, seconde,...
// Retourne la date et l'heure
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
horloge getDateDs1307() {
    // Reset the register pointer
    Wire.beginTransmission(DS1307_I2C_ADDRESS);
    Wire.write((uint8_t)0x00);
    Wire.endTransmission();
    Wire.requestFrom(DS1307_I2C_ADDRESS, 7);
    horloge HRLG;
    // A few of these need masks because certain bits are control bits
    HRLG.heureRTC.second     = bcdToDec(Wire.read() & 0x7f);
    HRLG.heureRTC.minute     = bcdToDec(Wire.read());
    HRLG.heureRTC.hour       = bcdToDec(Wire.read() & 0x3f);// Need to change this if 12 hour am/pm
    HRLG.dateRTC.dayOfWeek  = bcdToDec(Wire.read());
    HRLG.dateRTC.dayOfMonth = bcdToDec(Wire.read());
    HRLG.dateRTC.month      = bcdToDec(Wire.read());
    HRLG.dateRTC.year       = bcdToDec(Wire.read());
    return HRLG;
}
/*--------------------------------------------------------------------------------------------*/
// Déterminer le jour de la semaine (lundi, mardi, …, dimanche) 
// en fonction de la date, du mois et de l’année
/*--------------------------------------------------------------------------------------------*/
uint8_t Jour_Semaine( uint8_t d,uint8_t m,uint8_t y) {
  uint8_t c = (14 - m)/12;
  uint8_t a = y - c;
  uint8_t b = m + 12*c - 2;
  uint8_t day = ( d + a + a/4 - a/100 + a/400 + (31*b)/12 ) % 7;
  if (day == 0) day = 7;
  return day;
}
