// Fichier Test_Unitaire.c
// Tests



#include <Arduino.h>
#include "Test_Unitaire.h"
#include "GPS.h"
#include <assert.h>
extern char buff[256];
//"$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n" 

/*--------------------------------------------------------------------------------------------*/
// Test
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
void Test_Synchro(){
  //char buff[256];
  char bufferParse[16][32];
  //Choix du message NMEA en $GPCGGA
  //Serial.println("Choix du message NMEA en $GPCGGA");
  //Choix_Msg_NMEA("$GPGGA");

  //Choix du message NMEA en $GPRMC
  //Serial.println("Choix du message NMEA en $GPRMC");
  //Choix_Msg_NMEA("$GPRMC");

  //Implémentation du message NMEA dans un buffer
  Serial.println("Implémentation du message NMEA dans un buffer :");
  GetGPS_Msg();
  Serial.println(buff);
  //Serial.println();

  //Message NMEA parsé
  GetGPS_Msg_parse(bufferParse);

  //Affichage du message parsé
  Serial.println("Affichage du message parsé : ");
  for(int i =0; i<16;i++){
    int j = 0;
    while(bufferParse[i][j] != '\0'){
      Serial.print(bufferParse[i][j]);
      j++;
    }
    Serial.print("|");
  }
  Serial.println();
  //Serial.println();

  //Test de validation du message NMEA 
  if(Test_Synchro_GPS(bufferParse)){
    Serial.println("Message NMEA : Valide");
  }else {
    Serial.println("Message NMEA : Invalide");
  }
  //Serial.println();
  
}

bool Test_GPRMC(char bufferParse[16][32]){
  //Choix du message NMEA en $GPRMC
  //Serial.println("Choix du message NMEA en $GPCRMC");
  Choix_Msg_NMEA("$GPRMC");
  if(bufferParse[0] == "$GPRMC"){
    return 1;
  }else {
    return 0;
  }
  
}
