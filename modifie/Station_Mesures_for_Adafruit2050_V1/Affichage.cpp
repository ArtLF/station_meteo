// Fichier Affichage.cpp

// Routines d'affichage sur le terminal série
//   Affichage de la date et de l'heure sur terminal série
//   Affichage Indicateur Ete-Hiver sur le terminal série
//   Affichage Ville de référence fuseau horaire sur le terminal série
//   Affichage Indicateur de synchro sur le terminal série
//   Affichage des mesures du capteur BME680 sur le terminal série
//   Affichage de l'historique des mesures de valeur moyenne de pression atmosphérique sur le terminal série

#include <Arduino.h>
#include "Affichage.h"
#include "RTC_DS1307.h"

// Pour gestion de l'affichage de la date depuis le circuit RTC

/*--------------------------------------------------------------------------------------------*/
// Affichage de la date et de l'heure sur le terminal série
/*--------------------------------------------------------------------------------------------*/
void Affiche_date_heure(horloge HRLG){
 
  char *jour[7]={"Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"};
  char *mois[12]={"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"};


   Serial.print(jour[HRLG.dateRTC.dayOfWeek - 1]);
   Serial.print(" ");
   Serial.print(HRLG.dateRTC.dayOfMonth);
   Serial.print(" ");
   Serial.print(mois[HRLG.dateRTC.month - 1]);
   Serial.print(" ");
   Serial.print(HRLG.dateRTC.year + 2000);
   Serial.print(" ");
   Serial.print(HRLG.heureRTC.hour);
   Serial.print(" : ");
   Serial.print(HRLG.heureRTC.minute);
   Serial.print(" : ");
   Serial.print(HRLG.heureRTC.second);
   Serial.print("\n");
}

/*--------------------------------------------------------------------------------------------*/
// Afficher jour de la semaine (lundi, mardi, …, dimanche) 
// en fonction de la date, du mois et de l’année
/*--------------------------------------------------------------------------------------------*/
void Afficher_jour( uint8_t d,uint8_t m,uint8_t y){
  char *jour[7]={"Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"};

  Serial.print(jour[Jour_Semaine(d,m,y) - 1]);
}

/*--------------------------------------------------------------------------------------------*/
// Affichage de la date et de l'heure sur le terminal série
/*--------------------------------------------------------------------------------------------*/
// Affichage indicateur heure Eté - hiver sur le terminal série
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
/*--------------------------------------------------------------------------------------------*/
// Affichage Ville de référence du fuseau horaire sur le terminal série
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
/*--------------------------------------------------------------------------------------------*/
// Affichage de l'état de synchronisation du module GPS  sur le terminal série
// Si Synchro_GPS == 0 : pas de synchro;
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
/*--------------------------------------------------------------------------------------------*/
// Affichage des données du capteur  sur le terminal série
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
/*--------------------------------------------------------------------------------------------*/
// Afficher les données du graphe d'évolution de la valeur moyenne de la pression atmosphérique
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
/*--------------------------------------------------------------------------------------------*/
