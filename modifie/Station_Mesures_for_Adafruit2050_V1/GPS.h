// Fichier GPS.h

#include "RTC_DS1307.h"

// Définition des types de données pour gestion des données GPS


// Définition de constantes symboliques


// Fonctions prototypes
void GetGPS_Msg();

void GetGPS_Msg_parse(char bufferParse[16][32]);

bool Test_Synchro_GPS(char bufferParse[16][32]);

void Choix_Msg_NMEA(char PMTK[6]);

horloge Extract_date_heure_from_GPS(char bufferUTCparse[16][32]);
