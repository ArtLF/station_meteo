// Fichier GPS.c
// Fonction de gestion du module GPS
// Acquisition d'un message NMEA
// Parser un message NMEA
// Savoir si le module GPS est synchronisé
// Définir le type de message NMEA renvoyé par le module GPS
// Extraire les données de date et d'heure depuis les données GPS



#include <Arduino.h>
#include "GPS.h"
#include "RTC_DS1307.h"
extern char buff[256];
//"$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n" 
/*
Contenu du tableau de chaînes de caractères GPGGA_Data
  [0] nmea_msg; // Nom du message
  [1] heure;
  [2] latitude;
  [3] hemisphere; // Nord (N) ou Sud (S)
  [4] longitude;
  [5] longitude_orientation; // Est (E) ou Ouest (O)
  [6] validation des données : (1 ou 2) active ou 0 void (invalide);
  [7] satelites used; // Nombre de satellite utilisés;
  [8] HDOP; // Horizontal Dilution of Precision
  [9] Altitude;
  [10] Unité pour l'altitude; // M pour mètre
  [11] Geoid separation;
  [12] Unité pour geoid separation : M pour mètre
  [13] Age of Diff. Corr
  [14]Diff. Ref. Station ID

Contenu du tableau de chaînes de caractéres GPRMC_Data
  [0] nmea_msg; // Nom du message
  [1] heure;
  [2] validation des données : A active ou V void (invalide);
  [3] latitude;
  [4] hemisphere; // Nord (N) ou Sud (S)
  [5] longitude;
  [6] longitude_orientation; // Est (E) ou Ouest (O)
  [7] speed; // Speed over the ground in knots;
  [8] track; // Track angle in degrees True; Angle de déplacement
  [9] date;
  [10] magnet_declin; // Déclinaison magnétique en degrés
  [11] magnet_declin_orientation; // Est ou ouest
  
  Champs 10 et 11 non disponibles de base sur le module SIM28
*/

/*--------------------------------------------------------------------------------------------*/
// Fonctions de gestion du module GPS

/*--------------------------------------------------------------------------------------------*/
// Récupérer message depuis le module GPS
  
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
void GetGPS_Msg(){
  memset(buff, 0, 256);
  int i = 1;
  char data = ' ';
    while (data != '$'){
      if (Serial1.available()){
        data = char(Serial1.read());
      }
    }
      buff[0]=data;
      while( i<255 && (data != '*')){
        if (Serial1.available()){
          data = char(Serial1.read());
          buff[i]= data;
          }
      i++;
      }
     buff[i]='\0';
     
     /*
     int i = 0;
     do{
      buff[i++] = char(Serial1.read());
     } while (Serial1.available());
     buff[i+1] = '\0';*/
     //Serial.print(i);
}
/*--------------------------------------------------------------------------------------------*/
// Parser un message NMEA
// Paramètre d'entrée : le message NMEA à parser
// Valeur retournée : le tableau de chaines de caractéres des différents champs du message NMEA
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
void GetGPS_Msg_parse(char bufferParse[16][32]){
  memset(bufferParse,0,16*32);
  int i = 0;
  int j = 0;
  int k = 0;
  for(j; j<16;j++){
    bufferParse[j][0]= '\0';
  }
  j=0;
  while(buff[i]!='\0') {
    if(buff[i] == ','){
      bufferParse[j][k]= '\0';
      i++;
      j++;
      k=0;
    }else {
      bufferParse[j][k]= buff[i];
      i++;
      k++;
    }
  }
}
/*--------------------------------------------------------------------------------------------*/
// Test pour savoir si le module GPS reçoit des messages corrects
// Valeur retournée : boolean flag
//  flag = 0 si pas de synchronisation, flag = 1 si synchronisation
// Valable pour les messages de type GPRMC et GPGGA
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
bool Test_Synchro_GPS(char bufferParse[16][32]){
  if (bufferParse[2][0]=='A'||bufferParse[6][0]=='1'||bufferParse[6][0]=='2') return 1;
  else return 0;
  
}

/*--------------------------------------------------------------------------------------------*/
// Choix du type de message retourné par le module GPS
// Paramètre : la commande PMTK sous la forme d'une chaine de caractères
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
void Choix_Msg_NMEA(char PMTK[6]){
  if(PMTK=="$GPRMC"){
  Serial1.print("$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n");
  }
  if(PMTK=="$GPGGA"){
  Serial1.print("$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n");
  }
}
/*--------------------------------------------------------------------------------------------*/
// Extraction de la date et de l'heure depuis les données GPS
// Date au format jjmmyy : chaine de caractéres
// Heure au format hhmmss : chaine de caractéres en temps UTC
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
horloge Extract_date_heure_from_GPS(char bufferUTCparse[16][32]){
  horloge buffdateheure;
  // heure
  buffdateheure.heureRTC.hour=(uint8_t(bufferUTCparse[1][0])-'0')*10+uint8_t(bufferUTCparse[1][1])-'0';
  buffdateheure.heureRTC.minute=(uint8_t(bufferUTCparse[1][2])-'0')*10+uint8_t(bufferUTCparse[1][3])-'0';
  buffdateheure.heureRTC.second=(uint8_t(bufferUTCparse[1][4])-'0')*10+uint8_t(bufferUTCparse[1][5])-'0';

  // date
  buffdateheure.dateRTC.dayOfMonth=(uint8_t(bufferUTCparse[9][0])-'0')*10+uint8_t(bufferUTCparse[9][1])-'0';
  buffdateheure.dateRTC.month=(uint8_t(bufferUTCparse[9][2])-'0')*10+uint8_t(bufferUTCparse[9][3])-'0';
  buffdateheure.dateRTC.year=(uint16_t(bufferUTCparse[9][4])-'0')*10+uint16_t(bufferUTCparse[9][5])-'0';

  buffdateheure.dateRTC.dayOfWeek=Jour_Semaine(buffdateheure.dateRTC.dayOfMonth,buffdateheure.dateRTC.month,buffdateheure.dateRTC.year);

  return buffdateheure;
}
/*--------------------------------------------------------------------------------------------*/
// Conversion coordonnées géographiques GPS en dd°.xxxxxx
/*--------------------------------------------------------------------------------------------*/
// Ici votre code
/*--------------------------------------------------------------------------------------------*/
