// Fichier Synchro_IT_Timer.ino
// Exemple de mise en oeuvre de gestion temporelle d'événements
//  en utilisant les IT Timer1

#include "Evenement1.h"
#include "Evenement2.h"

#define T_EVNT1 4 // Période de gestion de l'événémént 1
#define T_EVNT2 8 // Période de gestion de l'événémént 2
#define TCNT1_TIMER1 49910U // Période entre 2 IT Timer1 sur Overflow registre de comptage (environ 1s)

volatile int T_Time_Out_Evenement1 = 0;
volatile int T_Time_Out_Evenement2 = 0;
int Id_instance_Evnt1 = 0, Id_instance_Evnt2 = 0;
unsigned long myTime = 0;

boolean isOn = false;

/*--------------------------------------------------------------------------------------------*/
// Routine d'IT TImer1 sur Overflow registre de comptage
ISR(TIMER1_OVF_vect)
{

  TIMSK1 &= B11111110; // Bloquer les IT Timer 1
  /*                                           
  T_Time_Out_Evenement1 --;
  T_Time_Out_Evenement2 --;
  */
  TIFR1 |= B00000001; // Réarmer à 1 le flag TOV1 de l'IT Timer 1
  TCNT1 = TCNT1_TIMER1; // Recharger le registre de comptage du Timer 1 
  TIMSK1 |= B0000001; // Autoriser les IT Timer 1
  SREG |= B10000000; // Autoriser toutes les IT/
  
  
  if(isOn){
    isOn=false;
  }
  else{
    isOn=true;
  }
}
/*--------------------------------------------------------------------------------------------*/

void setup ()
{
  Serial.begin (115200);
  
	// Initialisation des registres du Timer1
  //  - Définition d'une base de temps de 0.25s
  //  - Utilisation Timer1 avec IT sur débordement du registre de comptage
  noInterrupts(); // Bloquer toutes les interruptions
  TCCR1A = B00000000; // TCCR1A = Ox00 Mode normal
  TCCR1B = B00000000; // Mode normal, Timer 1 arrété
  TCCR1C = 0B00000000;
  TIFR1 |= B00000001; // Pour que les IT Timer 1 puissent être prises en compte; Mise à 1 du bit TOV1
  TIMSK1 |= B00000001; // Pour autoriser les IT sur débordement Timer 1; Mise à 1 du bit TOEI1
  TCNT1 = TCNT1_TIMER1; // Charger le contenu du registre de comptage du Timer 1
  interrupts (); // Autoriser toutes les interruptions
  TCCR1B |= B00000101; // Pour définir valeur de prescaler Timer 1 à 1024 et démarrer le compteur

  myTime = millis();
  Serial.print ("myTime : ");
  Serial.println (myTime);
  Serial.println ("Fin setup");
}

void loop ()
{
  /*
	if (T_Time_Out_Evenement1 <= 0)
	{
	  Tache1 ();
    T_Time_Out_Evenement1 = T_EVNT1;
	}

  if (T_Time_Out_Evenement2 <= 0)
  {
    Tache2 ();
    T_Time_Out_Evenement2 = T_EVNT2;
  }
  */
  digitalWrite(2, isOn);
}
